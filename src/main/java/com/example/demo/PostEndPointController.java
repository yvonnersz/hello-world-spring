package com.example.demo;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class PostEndPointController {
    @PostMapping("/search")
    public String searchAndReplace(@RequestParam(required = true) String body, @RequestParam(required = true) String search, @RequestParam(required = true) String replace) {
        return body.replaceAll(search, replace);
    }
}
