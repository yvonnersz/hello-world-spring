package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class ComputeValueController {
    @GetMapping("/compute")

    public String computeValue(@RequestParam(required = true) double num1, @RequestParam(required = true) double num2, @RequestParam(required = true) String operator) {
        if (operator.equals("add")) {
            return String.valueOf(num1 + num2);
        } else if (operator.equals("minus")) {
            return String.valueOf(num1 - num2);
        } else if (operator.equals("multiply")) {
            return String.valueOf(num1 * num2);
        } else if (operator.equals("divide")) {
            return String.valueOf(num1 / num2);
        }

        return "error";
    }
}
