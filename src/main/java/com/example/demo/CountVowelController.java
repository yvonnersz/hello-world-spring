package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController

public class CountVowelController {
    @GetMapping("/vowels")

    public String countVowels(@RequestParam(required = true) String word) {
        int numberOfVowels = 0;
        String lowercaseWord = word.toLowerCase(Locale.ROOT);

        for (int i = 0; i < lowercaseWord.length(); i++) {
            char letter = lowercaseWord.charAt(i);

            if (letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
                numberOfVowels += 1;
            }
        }

        return String.valueOf(numberOfVowels);
    }

}
