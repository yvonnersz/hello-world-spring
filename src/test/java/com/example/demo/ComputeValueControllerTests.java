package com.example.demo;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(ComputeValueController.class)

public class ComputeValueControllerTests {
    @Autowired
    MockMvc mockMvc;

    @Test
    void computeValue_withNumsAndAddArg_returnsComputedValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?num1=5&num2=1&operator=add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("6.0"));
    }

    @Test
    void computeValue_withNumsAndMinusArg_returnsComputedValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?num1=5&num2=1&operator=minus"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("4.0"));
    }

    @Test
    void computeValue_withNumsAndMultiplyArg_returnsComputedValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?num1=5&num2=1&operator=multiply"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("5.0"));
    }

    @Test
    void computeValue_withNumsAndDivideArg_returnsComputedValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?num1=5&num2=1&operator=divide"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("5.0"));
    }

    @Test
    void computeValue_withNumsAndAddWithDecimalArg_returnsComputedValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?num1=5.2&num2=5.2&operator=add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("10.4"));
    }

    @Test
    void computeValue_withNumsAndMinusWithDecimalArg_returnsComputedValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?num1=5.2&num2=2.2&operator=minus"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("3.0"));
    }

    @Test
    void computeValue_withNumsAndMultiplyWithDecimalArg_returnsComputedValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?num1=5.2&num2=2&operator=multiply"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("10.4"));
    }

    @Test
    void computeValue_withNumsAndDivideWithDecimalArg_returnsComputedValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?num1=5.2&num2=5.2&operator=divide"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("1.0"));
    }

    @Test
    void computeValue_withNumsAndDivideWithRemainder_returnsComputedValue() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/compute?num1=5&num2=2&operator=divide"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("2.5"));
    }
}
