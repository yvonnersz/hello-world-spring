package com.example.demo;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(CountVowelController.class)

public class CountVowelControllerTests {
    @Autowired
    MockMvc mockMvc;

    @Test
    void countVowels_withStringArg_returnsNumbersOfValues() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/vowels?word=yvonne"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("2"));
    }
}
